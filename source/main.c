#define F_CPU 16000000U

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "waves.h"
#include "pow_table.h"



/** ******************** **/
/** OSCILLATOR VARIABLES **/
/** ******************** **/
uint32_t phase = 0;
int32_t phaseInc;
  
uint8_t wave[256 * 2]; 		// Slot for two waveforms
uint8_t waveNumber = 0; 	// Current waveform
int8_t previousWaveNumber = 0;
uint8_t finalWaveform[256]; 	// Output waveform 

uint8_t outputvalue = 255; 	// Sample written out to PWM
uint8_t counter = 0; 		// Limit the rate at which slot and crossfade input adc is read

uint16_t analogInput[8] = {0};	// Stores the values of the 8 analog inputs
uint32_t freqCV = 0;		
float fadeCV = 0;


// FX variables
uint8_t fxNumber = 0;		// Number of Selected FX
uint16_t fxCV = 0;		// Fx control
uint8_t bitDepthCV = 0;		// Store the bit depth if this effect is selected
uint8_t bitDepthMask = 0;	// Mask to generate bit-crushed waves
uint8_t fxSampleRate = 0;	// Sample rate of the modulating effects
/** ~OSCILLATOR VARIABLES **/




/** *************************** **/
/**	SETUP FUNCTIONS		**/
/** *************************** **/
void setupADC (void) {
  ADMUX |= _BV(REFS0);		// AVCC Reference, channel 0
  ADCSRA |= _BV(ADIE);		// Enable ADC Interrupt 
  //ADCSRA |= _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);	// Prescaler: 128
  //ADCSRA |= _BV(ADPS1) | _BV(ADPS2);			// Prescaler: 64
  ADCSRA |= _BV(ADPS0) | _BV(ADPS2);			// Prescaler: 32
  //ADCSRA |= _BV(ADPS2);				// Prescaler: 16
  DIDR0 = 0xFF;			// Disable digital input buffers to reduce power consumption
  ADCSRA |= _BV(ADATE);	// Autotrigger enable		
  ADCSRA |= _BV(ADEN);		// Enable ADC
  ADCSRA |= _BV(ADSC);		// Start first conversion
}

void setupTimer (void) {
  TCCR2A |= _BV(COM2A1);		// PIND7 as PWM output (OCR2A) 
  TCCR2A |= _BV(WGM20) | _BV(WGM21);	// Fast PWM Mode
  TCCR2B |= _BV(CS20);			// Prescaler: No Prescaling
  TIMSK2 |= _BV(TOIE2);			// Enable overflow interrupt
}

void setupInterrupts (void) {
  // INT0 (FX)
  EICRA = _BV(ISC01);			// Falling Edge
  EIMSK = _BV(INT0);			// Enable External Interrupt 0

  // INT1 (Sync)
  EICRA = _BV(ISC11) | _BV(ISC10);	// Rising Edge
  EIMSK = _BV(INT1); 			// Enable External Interrupt 1
}

void setupPorts (void) {
  DDRD |= _BV(PIND7);			// PWM pin as output
  PORTD |= _BV(PIND2) | _BV(PIND3); 	// Pull-Up for external interrupt pins
}
/**	~SETUP FUNCTIONS	**/




void changeWave (uint8_t w) {
  for (uint16_t i = 0; i < DOUBLE_TABLE_SIZE; i++) 
    wave[i] = pgm_read_byte_near(waveTable + w*TABLE_SIZE + i);  
}

unsigned long phaseinc(float freq) {
  return 536.870912*freq;
  //return (1<<8)*(1l<<16) * freq / (F_CPU/timerPrescale);
}


uint16_t analogRead(uint8_t ADCInput) {
  ADMUX = 0x40 | ADCInput;
  ADCSRA |= _BV(ADSC);
  while (bit_is_set(ADCSRA, ADSC));
  uint8_t lowBit = ADCL;
  return (ADCH << 8) | lowBit;
}


/** ******************** **/
/**	MAIN BLOCK	 **/
/** ******************** **/
void main (void) {


  setupPorts();
  setupTimer();
  setupADC();
  setupInterrupts();
  changeWave(0);
  sei();

  while (1) {
          
    freqCV = (pgm_read_dword_near(pow_table + (int)(2*analogInput[0])) / 1024.0) + 60;
    phaseInc = phaseinc(freqCV);
    
    ADCSRA |= _BV(ADSC);

    /** Only the frequency control is updated every cycle. Other controls
    are updated only every 50th cycle **/
    if(counter++ >= 200){
      
      
while (bit_is_set(ADCSRA, ADSC));
      /** Loading new waveform if necessary **/
      waveNumber = (int)(analogInput[1]/10*0.2 + waveNumber*0.8);
      ADMUX = 0x41;	// Analog 1
      ADCSRA |= _BV(ADSC);
      if (waveNumber != previousWaveNumber) {
	changeWave(waveNumber);
	previousWaveNumber = waveNumber;
      }
      
            
       /** Fade between two waves **/
      fadeCV = analogInput[2] * 0.0009775;	// 0 to 1
      ADMUX = 0x42;	// Analog 2
      ADCSRA |= _BV(ADSC);
      
     
      
      /** FX SECTION **/
      
      fxCV = analogInput[3]*0.8 + fxCV*0.2;
      ADMUX = 0x43;	// Analog 3
      ADCSRA |= _BV(ADSC);
    switch (fxNumber) {
      case 0:     // Bit depth  
        bitDepthCV = (fxCV / 4)*0.2 + bitDepthCV*0.8;
        bitDepthMask = 0;
        for (uint8_t i = bitDepthCV / 32; i < 8; i++)
          bitDepthMask |= (1 << i);
        for(int i = 0; i < TABLE_SIZE; ++i)
          finalWaveform[i] =  (uint8_t)((wave[i] * fadeCV) + (wave[i + TABLE_SIZE] * (1 - fadeCV))) & bitDepthMask;
        break;
      
      case 1:    // Sample Kill
        fxSampleRate = fxCV/10;
        for(int i = 0; i < TABLE_SIZE; ++i)
          if ((i/fxSampleRate)%2)
            finalWaveform[i] =  (uint8_t)((wave[i] * fadeCV) + (wave[i + TABLE_SIZE] * (1 - fadeCV))); 
          else
            finalWaveform[i] =  0; 
        break;
        
      case 2:    // Sample Polarity
        fxSampleRate = fxCV/10;
        for(int i = 0; i < TABLE_SIZE; ++i)
          if ((i/fxSampleRate)%2)
            finalWaveform[i] =  (uint8_t)((wave[i] * fadeCV) + (wave[i + TABLE_SIZE] * (1 - fadeCV))); 
          else
            finalWaveform[i] = 255 - (uint8_t)((wave[i] * fadeCV) + (wave[i + TABLE_SIZE] * (1 - fadeCV))); 
        break;
           
      case 3: {    // Sample Rate
        fxSampleRate = fxCV*0.2463+4;
        int n = (TABLE_SIZE/fxSampleRate);
        for(int i = 0; i < TABLE_SIZE; ++i)
          finalWaveform[i] =  (wave[(i/n)*n] * fadeCV) + (wave[((i+TABLE_SIZE)/n)*n] * (1 - fadeCV));
        break;   
      }
      default:
	for(int i = 0; i < TABLE_SIZE; ++i)
          finalWaveform[i] =  (uint8_t)((wave[i] * fadeCV) + (wave[i + TABLE_SIZE] * (1 - fadeCV)));
        break;
     
    };
      
     
      ADMUX = 0x40;	// Analog 0
      counter = 0;

    }
   
   
  }
}
/**	~MAIN BLOCK	 **/




/** *********************************** **/
/**	INTERRUPTION SERVICE ROUTINES	**/
/** *********************************** **/


ISR (ADC_vect) {
  /** The values of the 8 voltage controls are stored
      in the "analogInput[]" variable **/
  uint8_t lowBit = ADCL;
  analogInput[(ADMUX & 0x03)] = (ADCH << 8) | lowBit;
}

ISR (TIMER2_OVF_vect) {
  /** Output the actual value and increment the phase **/
  OCR2A = outputvalue; 
  outputvalue = (finalWaveform[phase>>24]); 
  phase = phase + phaseInc;
}

/** FX button interrupt **/
ISR(INT1_vect) {
  if (++fxNumber >= FX_QTY) fxNumber = 0;	//  If button pressed, change to next effect
}


/** Sync interruption **/
ISR (INT2_vect) {
  phase = 0;
}
/**	~INTERRUPTION SERVICE ROUTINES	**/
