EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:dac
LIBS:wavetable_vco-cache
EELAYER 24 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 2
Title "Wavetable VCO"
Date "6 jul 2013"
Rev "4"
Comp "No-FI"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 7900 7050
Connection ~ 8700 7050
Wire Wire Line
	4550 6500 4300 6500
Wire Wire Line
	4300 6500 4300 6300
Wire Wire Line
	4300 6300 4150 6300
Wire Wire Line
	4550 6300 4400 6300
Wire Wire Line
	4400 6300 4400 5800
Wire Wire Line
	4400 5800 4150 5800
Connection ~ 10750 4400
Wire Wire Line
	10750 4400 10750 3450
Wire Wire Line
	6550 7200 6550 7150
Wire Wire Line
	6550 7150 6450 7150
Wire Wire Line
	6450 6850 6500 6850
Wire Wire Line
	5350 7150 5650 7150
Wire Wire Line
	5350 5800 5350 7150
Wire Wire Line
	2200 7100 1900 7100
Wire Wire Line
	9700 6700 9450 6700
Connection ~ 9250 3050
Wire Wire Line
	9250 4100 9250 3050
Wire Wire Line
	9250 4100 7500 4100
Wire Wire Line
	7500 4100 7500 4800
Wire Wire Line
	8150 4300 9800 4300
Wire Wire Line
	8750 4400 8750 4300
Wire Wire Line
	8150 4300 8150 4350
Wire Wire Line
	8750 4800 8750 4900
Wire Wire Line
	8150 5350 8150 5500
Wire Wire Line
	8700 4900 8800 4900
Connection ~ 9800 5000
Connection ~ 8750 4900
Wire Wire Line
	8100 4900 8200 4900
Connection ~ 8150 4900
Wire Wire Line
	8150 4850 8150 4950
Wire Wire Line
	8800 5100 8800 5500
Connection ~ 8750 4300
Wire Wire Line
	9800 4300 9800 5000
Wire Wire Line
	9800 5000 9900 5000
Wire Wire Line
	9250 3050 8900 3050
Connection ~ 5500 5900
Wire Wire Line
	4150 5900 5750 5900
Wire Wire Line
	6250 5900 6700 5900
Connection ~ 5350 6000
Wire Wire Line
	1850 5850 1850 5950
Wire Wire Line
	14900 4100 14900 3900
Wire Wire Line
	3000 2700 3000 2600
Wire Wire Line
	4600 4250 4550 4250
Connection ~ 4550 4500
Wire Wire Line
	4550 4450 4550 4550
Wire Wire Line
	4550 4450 4600 4450
Wire Wire Line
	4550 4700 4450 4700
Wire Wire Line
	4450 4700 4450 4600
Wire Wire Line
	4450 4600 4150 4600
Wire Wire Line
	4600 4650 4550 4650
Wire Wire Line
	12700 5100 12800 5100
Wire Wire Line
	14900 3900 14950 3900
Wire Wire Line
	9250 2550 9050 2550
Wire Wire Line
	9050 2550 9050 2700
Wire Wire Line
	10450 7100 10450 7200
Wire Wire Line
	9200 7750 10350 7750
Wire Wire Line
	9200 7350 9200 7750
Wire Wire Line
	12200 5100 12100 5100
Wire Wire Line
	12100 4400 12100 5100
Wire Wire Line
	13900 3050 14150 3050
Connection ~ 1900 3000
Wire Wire Line
	2750 8550 2750 8150
Wire Wire Line
	6800 2800 6700 2800
Wire Wire Line
	6650 3400 6650 3750
Wire Wire Line
	6650 3400 6800 3400
Wire Wire Line
	9050 2700 8900 2700
Connection ~ 8700 7550
Wire Wire Line
	7900 7550 9200 7550
Connection ~ 8700 7350
Wire Wire Line
	7900 7350 9200 7350
Wire Wire Line
	10350 7750 10350 7850
Connection ~ 10050 7250
Wire Wire Line
	10050 7100 10050 7300
Connection ~ 10050 8250
Wire Wire Line
	10050 8200 10050 8350
Wire Wire Line
	9950 8250 10050 8250
Connection ~ 9200 7550
Wire Wire Line
	8900 3200 9000 3200
Wire Wire Line
	7500 3700 7500 3800
Wire Wire Line
	12150 2950 12550 2950
Wire Wire Line
	12550 2950 12550 3550
Wire Wire Line
	11250 3450 11550 3450
Wire Wire Line
	11550 3650 11550 3900
Wire Wire Line
	13150 3750 13150 4050
Wire Wire Line
	3000 6950 3000 6800
Wire Wire Line
	3000 6800 3100 6800
Wire Wire Line
	2750 2100 2750 2200
Wire Wire Line
	2750 2200 2700 2200
Connection ~ 11050 4400
Wire Wire Line
	11100 5200 11100 5600
Wire Wire Line
	10450 4950 10450 5050
Connection ~ 14150 3650
Connection ~ 10450 5000
Wire Wire Line
	10400 5000 10500 5000
Connection ~ 11050 5000
Connection ~ 3150 2600
Wire Wire Line
	3150 2450 3150 2600
Wire Wire Line
	4150 3300 5800 3300
Wire Wire Line
	5800 3300 5800 3000
Wire Wire Line
	4150 3100 4900 3100
Wire Wire Line
	4900 3100 4900 3000
Wire Wire Line
	4900 3000 4950 3000
Wire Wire Line
	5100 3250 5100 3400
Wire Wire Line
	5100 2600 5100 2750
Wire Wire Line
	4150 3000 4500 3000
Wire Wire Line
	4650 2600 4650 2750
Wire Wire Line
	2150 4200 1900 4200
Wire Wire Line
	1900 4200 1900 4350
Wire Wire Line
	1750 3000 2150 3000
Wire Wire Line
	800  4350 800  4450
Connection ~ 800  3800
Wire Wire Line
	800  3400 800  3950
Wire Wire Line
	2150 3800 1400 3800
Wire Wire Line
	1950 8950 1950 9250
Wire Wire Line
	1950 8150 1950 8550
Wire Wire Line
	800  3400 2150 3400
Wire Wire Line
	1400 3800 1400 3950
Wire Wire Line
	1400 4350 1400 4450
Connection ~ 1400 3800
Wire Wire Line
	1900 4750 1900 4950
Wire Wire Line
	4650 3250 4650 3400
Wire Wire Line
	5550 2600 5550 2750
Wire Wire Line
	5550 3250 5550 3400
Wire Wire Line
	5400 3000 5350 3000
Wire Wire Line
	5350 3000 5350 3200
Wire Wire Line
	5350 3200 4150 3200
Wire Wire Line
	5800 3000 5850 3000
Wire Wire Line
	6000 3250 6000 3400
Wire Wire Line
	6000 2600 6000 2750
Wire Wire Line
	3100 6800 3100 6700
Wire Wire Line
	3200 6700 3200 6800
Wire Wire Line
	8250 3700 8250 3800
Connection ~ 12100 5100
Wire Wire Line
	11000 5000 11100 5000
Wire Wire Line
	14350 3650 14150 3650
Wire Wire Line
	10450 5450 10450 5600
Wire Wire Line
	11050 4900 11050 5000
Wire Wire Line
	10450 4450 10450 4400
Wire Wire Line
	11050 4400 11050 4500
Wire Wire Line
	2700 2400 2750 2400
Wire Wire Line
	2750 2400 2750 2550
Wire Wire Line
	3200 6800 3300 6800
Wire Wire Line
	3300 6800 3300 6950
Wire Wire Line
	14850 3650 14950 3650
Connection ~ 12550 3550
Wire Wire Line
	2350 8950 2350 9250
Wire Wire Line
	2350 8150 2350 8550
Wire Wire Line
	11250 2950 11650 2950
Wire Wire Line
	11450 2950 11450 3450
Connection ~ 11450 2950
Connection ~ 11450 3450
Wire Wire Line
	10650 2950 10750 2950
Wire Wire Line
	7750 3700 7750 3800
Wire Wire Line
	8000 3700 8000 3800
Connection ~ 9200 7450
Wire Wire Line
	9950 7250 10050 7250
Wire Wire Line
	9050 7650 9050 8250
Wire Wire Line
	9050 8250 9450 8250
Wire Wire Line
	10050 7700 10050 7800
Connection ~ 10050 7750
Wire Wire Line
	7900 7250 9450 7250
Connection ~ 8700 7250
Wire Wire Line
	7900 7450 9200 7450
Connection ~ 8700 7450
Wire Wire Line
	7900 7650 9050 7650
Connection ~ 8700 7650
Connection ~ 8700 7150
Wire Wire Line
	9000 3200 9000 3450
Wire Wire Line
	6500 3250 6800 3250
Wire Wire Line
	6650 3750 6500 3750
Connection ~ 6650 3500
Wire Wire Line
	6800 3100 6500 3100
Wire Wire Line
	6500 3100 6500 3050
Wire Wire Line
	6800 2650 6700 2650
Wire Wire Line
	6700 2650 6700 2850
Connection ~ 6700 2800
Wire Wire Line
	2750 9250 2750 8950
Wire Wire Line
	1900 2400 1900 3000
Wire Wire Line
	10450 4400 12100 4400
Wire Wire Line
	13150 3050 13150 3550
Wire Wire Line
	13150 3050 13400 3050
Wire Wire Line
	13150 3550 13050 3550
Connection ~ 13150 3550
Wire Wire Line
	7900 7150 9450 7150
Wire Wire Line
	9450 7150 9450 6700
Wire Wire Line
	10700 6700 10700 6650
Connection ~ 10450 6700
Wire Wire Line
	4550 4750 4600 4750
Wire Wire Line
	4550 4650 4550 4750
Connection ~ 4550 4700
Wire Wire Line
	4150 4500 4550 4500
Wire Wire Line
	4550 4550 4600 4550
Wire Wire Line
	4150 4400 4450 4400
Wire Wire Line
	4450 4400 4450 4300
Wire Wire Line
	4450 4300 4550 4300
Wire Wire Line
	4550 4350 4600 4350
Wire Wire Line
	4550 4250 4550 4350
Connection ~ 4550 4300
Wire Wire Line
	3000 2600 3300 2600
Wire Wire Line
	3300 2600 3300 2700
Wire Wire Line
	12800 5350 12750 5350
Wire Wire Line
	12750 5350 12750 5550
Wire Wire Line
	6450 6950 6700 6950
Wire Wire Line
	6700 6950 6700 5900
Wire Wire Line
	1850 5450 1850 5350
Wire Wire Line
	5350 5300 5350 5100
Wire Wire Line
	5500 6300 5500 6350
Wire Wire Line
	7500 4900 7600 4900
Wire Wire Line
	7500 5000 7500 5100
Wire Wire Line
	7500 5100 7600 5100
Wire Wire Line
	1250 3000 1150 3000
Wire Wire Line
	1150 3000 1150 2850
Wire Wire Line
	10200 6700 10700 6700
Wire Wire Line
	6450 7050 8700 7050
Wire Wire Line
	2200 7100 2200 7250
Wire Wire Line
	1900 7100 1900 7250
Wire Wire Line
	4150 6000 5350 6000
Wire Wire Line
	6450 6750 6550 6750
Wire Wire Line
	6550 6750 6550 6700
Wire Wire Line
	14150 3650 14150 3050
Wire Wire Line
	4150 5700 4450 5700
Wire Wire Line
	4450 5700 4450 6200
Wire Wire Line
	4450 6200 4550 6200
Wire Wire Line
	4150 6100 4350 6100
Wire Wire Line
	4350 6100 4350 6400
Wire Wire Line
	4350 6400 4550 6400
$Comp
L CONN_4 J13
U 1 1 51D875C3
P 4900 6350
F 0 "J13" V 4850 6350 50  0000 C CNN
F 1 "LEDS" V 4950 6350 50  0000 C CNN
F 2 "" H 4900 6350 60  0001 C CNN
F 3 "" H 4900 6350 60  0001 C CNN
	1    4900 6350
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR01
U 1 1 51D79289
P 6500 6850
F 0 "#PWR01" H 6500 6850 40  0001 C CNN
F 1 "AGND" H 6500 6780 50  0000 C CNN
F 2 "" H 6500 6850 60  0001 C CNN
F 3 "" H 6500 6850 60  0001 C CNN
	1    6500 6850
	0    -1   -1   0   
$EndComp
$Comp
L DGND #PWR02
U 1 1 51D76348
P 1900 7250
F 0 "#PWR02" H 1900 7250 40  0001 C CNN
F 1 "DGND" H 1900 7180 40  0000 C CNN
F 2 "" H 1900 7250 60  0001 C CNN
F 3 "" H 1900 7250 60  0001 C CNN
	1    1900 7250
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR03
U 1 1 51D76347
P 2200 7250
F 0 "#PWR03" H 2200 7250 40  0001 C CNN
F 1 "AGND" H 2200 7180 50  0000 C CNN
F 2 "" H 2200 7250 60  0001 C CNN
F 3 "" H 2200 7250 60  0001 C CNN
	1    2200 7250
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 51D731AB
P 9950 6700
F 0 "R9" V 10030 6700 50  0000 C CNN
F 1 "10" V 9950 6700 50  0000 C CNN
F 2 "" H 9950 6700 60  0001 C CNN
F 3 "" H 9950 6700 60  0001 C CNN
	1    9950 6700
	0    1    1    0   
$EndComp
$Sheet
S 5750 6550 600  700 
U 51D72FD6
F0 "SUB PCB" 60
F1 "sub_pcb.sch" 60
$EndSheet
$Comp
L TL072 U?
U 1 1 51D72D7C
P 9300 5000
AR Path="/51326B47" Ref="U?"  Part="1" 
AR Path="/51326B51" Ref="U?"  Part="1" 
AR Path="/51328582" Ref="U?"  Part="1" 
AR Path="/51328590" Ref="U?"  Part="1" 
AR Path="/51D72D7C" Ref="U1"  Part="1" 
F 0 "U1" H 9250 5200 60  0000 L CNN
F 1 "TL072" H 9250 4750 60  0000 L CNN
F 2 "" H 9300 5000 60  0001 C CNN
F 3 "" H 9300 5000 60  0001 C CNN
	1    9300 5000
	1    0    0    1   
$EndComp
$Comp
L R R5
U 1 1 51D72D7B
P 8150 4600
F 0 "R5" V 8230 4600 50  0000 C CNN
F 1 "150K" V 8150 4600 50  0000 C CNN
F 2 "" H 8150 4600 60  0001 C CNN
F 3 "" H 8150 4600 60  0001 C CNN
	1    8150 4600
	-1   0    0    1   
$EndComp
$Comp
L C C8
U 1 1 51D72D7A
P 8150 5150
F 0 "C8" H 8200 5250 50  0000 L CNN
F 1 "100p" H 8200 5050 50  0000 L CNN
F 2 "" H 8150 5150 60  0001 C CNN
F 3 "" H 8150 5150 60  0001 C CNN
	1    8150 5150
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 51D72D79
P 8750 4600
F 0 "C11" H 8800 4700 50  0000 L CNN
F 1 "1n" H 8800 4500 50  0000 L CNN
F 2 "" H 8750 4600 60  0001 C CNN
F 3 "" H 8750 4600 60  0001 C CNN
	1    8750 4600
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 51D72D78
P 7850 4900
F 0 "R4" V 7930 4900 50  0000 C CNN
F 1 "150K" V 7850 4900 50  0000 C CNN
F 2 "" H 7850 4900 60  0001 C CNN
F 3 "" H 7850 4900 60  0001 C CNN
	1    7850 4900
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 51D72D77
P 8450 4900
F 0 "R8" V 8530 4900 50  0000 C CNN
F 1 "8K5" V 8450 4900 50  0000 C CNN
F 2 "" H 8450 4900 60  0001 C CNN
F 3 "" H 8450 4900 60  0001 C CNN
	1    8450 4900
	0    1    1    0   
$EndComp
$Comp
L AGND #PWR04
U 1 1 51D72D76
P 8150 5500
F 0 "#PWR04" H 8150 5500 40  0001 C CNN
F 1 "AGND" H 8150 5430 50  0000 C CNN
F 2 "" H 8150 5500 60  0001 C CNN
F 3 "" H 8150 5500 60  0001 C CNN
	1    8150 5500
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR05
U 1 1 51D72D75
P 8800 5500
F 0 "#PWR05" H 8800 5500 40  0001 C CNN
F 1 "AGND" H 8800 5430 50  0000 C CNN
F 2 "" H 8800 5500 60  0001 C CNN
F 3 "" H 8800 5500 60  0001 C CNN
	1    8800 5500
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 51D6EFC1
P 6000 5900
F 0 "R3" V 6080 5900 50  0000 C CNN
F 1 "1K" V 6000 5900 50  0000 C CNN
F 2 "" H 6000 5900 60  0001 C CNN
F 3 "" H 6000 5900 60  0001 C CNN
	1    6000 5900
	0    1    1    0   
$EndComp
$Comp
L DGND #PWR06
U 1 1 51D6EDDA
P 5500 6350
F 0 "#PWR06" H 5500 6350 40  0001 C CNN
F 1 "DGND" H 5500 6280 40  0000 C CNN
F 2 "" H 5500 6350 60  0001 C CNN
F 3 "" H 5500 6350 60  0001 C CNN
	1    5500 6350
	1    0    0    -1  
$EndComp
$Comp
L ZENER D1
U 1 1 51D6EDBE
P 5500 6100
F 0 "D1" H 5500 6200 50  0000 C CNN
F 1 "5V1" H 5500 6000 40  0000 C CNN
F 2 "" H 5500 6100 60  0001 C CNN
F 3 "" H 5500 6100 60  0001 C CNN
	1    5500 6100
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR07
U 1 1 51D6DB3B
P 5350 5100
F 0 "#PWR07" H 5350 5190 20  0001 C CNN
F 1 "+5V" H 5350 5190 30  0000 C CNN
F 2 "" H 5350 5100 60  0001 C CNN
F 3 "" H 5350 5100 60  0001 C CNN
	1    5350 5100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR08
U 1 1 5194524A
P 1850 5350
F 0 "#PWR08" H 1850 5440 20  0001 C CNN
F 1 "+5V" H 1850 5440 30  0000 C CNN
F 2 "" H 1850 5350 60  0001 C CNN
F 3 "" H 1850 5350 60  0001 C CNN
	1    1850 5350
	1    0    0    -1  
$EndComp
$Comp
L DGND #PWR09
U 1 1 51945243
P 1850 5950
F 0 "#PWR09" H 1850 5950 40  0001 C CNN
F 1 "DGND" H 1850 5880 40  0000 C CNN
F 2 "" H 1850 5950 60  0001 C CNN
F 3 "" H 1850 5950 60  0001 C CNN
	1    1850 5950
	1    0    0    -1  
$EndComp
NoConn ~ 4150 6400
$Comp
L CONN_3 J4
U 1 1 519439CA
P 7150 4900
F 0 "J4" V 7100 4900 50  0000 C CNN
F 1 "OUT_SEL" V 7200 4900 40  0000 C CNN
F 2 "" H 7150 4900 60  0001 C CNN
F 3 "" H 7150 4900 60  0001 C CNN
	1    7150 4900
	-1   0    0    -1  
$EndComp
Text GLabel 7600 5100 2    40   Input ~ 0
PWM
NoConn ~ 12800 5200
NoConn ~ 14950 3750
$Comp
L VCC #PWR010
U 1 1 519429D8
P 6550 6700
F 0 "#PWR010" H 6550 6800 30  0001 C CNN
F 1 "VCC" H 6550 6800 30  0000 C CNN
F 2 "" H 6550 6700 60  0001 C CNN
F 3 "" H 6550 6700 60  0001 C CNN
	1    6550 6700
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR011
U 1 1 519429D7
P 6550 7200
F 0 "#PWR011" H 6550 7300 30  0001 C CNN
F 1 "VDD" H 6550 7310 30  0000 C CNN
F 2 "" H 6550 7200 60  0001 C CNN
F 3 "" H 6550 7200 60  0001 C CNN
	1    6550 7200
	-1   0    0    1   
$EndComp
Text GLabel 5650 6750 0    40   Output ~ 0
ADC3
Text GLabel 5650 6950 0    40   Output ~ 0
ADC1
Text GLabel 5650 6850 0    40   Output ~ 0
ADC2
Text GLabel 5650 7050 0    40   Output ~ 0
ADC0
Text GLabel 4150 5500 2    40   Output ~ 0
DAC7
$Comp
L CONN_5X2 J2
U 1 1 519429D9
P 6050 6950
F 0 "J2" H 6050 7250 60  0000 C CNN
F 1 "INTER PCB" V 6050 6950 50  0000 C CNN
F 2 "" H 6050 6950 60  0001 C CNN
F 3 "" H 6050 6950 60  0001 C CNN
	1    6050 6950
	1    0    0    -1  
$EndComp
$Comp
L JACK_2P J5
U 1 1 513746B8
P 13250 5200
F 0 "J5" H 12900 5000 60  0000 C CNN
F 1 "DC OUT" H 13100 5450 60  0000 C CNN
F 2 "" H 13250 5200 60  0001 C CNN
F 3 "" H 13250 5200 60  0001 C CNN
	1    13250 5200
	-1   0    0    1   
$EndComp
$Comp
L AGND #PWR012
U 1 1 513746B7
P 12750 5550
F 0 "#PWR012" H 12750 5550 40  0001 C CNN
F 1 "AGND" H 12750 5480 50  0000 C CNN
F 2 "" H 12750 5550 60  0001 C CNN
F 3 "" H 12750 5550 60  0001 C CNN
	1    12750 5550
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR013
U 1 1 513746A6
P 14900 4100
F 0 "#PWR013" H 14900 4100 40  0001 C CNN
F 1 "AGND" H 14900 4030 50  0000 C CNN
F 2 "" H 14900 4100 60  0001 C CNN
F 3 "" H 14900 4100 60  0001 C CNN
	1    14900 4100
	1    0    0    -1  
$EndComp
$Comp
L JACK_2P J6
U 1 1 51374636
P 15400 3750
F 0 "J6" H 15050 3550 60  0000 C CNN
F 1 "AC OUT" H 15250 4000 60  0000 C CNN
F 2 "" H 15400 3750 60  0001 C CNN
F 3 "" H 15400 3750 60  0001 C CNN
	1    15400 3750
	-1   0    0    1   
$EndComp
$Comp
L R R11
U 1 1 51373F53
P 9250 2800
F 0 "R11" V 9330 2800 50  0000 C CNN
F 1 "50" V 9250 2800 50  0000 C CNN
F 2 "" H 9250 2800 60  0001 C CNN
F 3 "" H 9250 2800 60  0001 C CNN
	1    9250 2800
	-1   0    0    1   
$EndComp
$Comp
L DGND #PWR014
U 1 1 5135FBB1
P 2750 2550
F 0 "#PWR014" H 2750 2550 40  0001 C CNN
F 1 "DGND" H 2750 2480 40  0000 C CNN
F 2 "" H 2750 2550 60  0001 C CNN
F 3 "" H 2750 2550 60  0001 C CNN
	1    2750 2550
	1    0    0    -1  
$EndComp
$Comp
L DGND #PWR015
U 1 1 5135FBA3
P 800 4450
F 0 "#PWR015" H 800 4450 40  0001 C CNN
F 1 "DGND" H 800 4380 40  0000 C CNN
F 2 "" H 800 4450 60  0001 C CNN
F 3 "" H 800 4450 60  0001 C CNN
	1    800  4450
	1    0    0    -1  
$EndComp
$Comp
L DGND #PWR016
U 1 1 5135FBA1
P 1400 4450
F 0 "#PWR016" H 1400 4450 40  0001 C CNN
F 1 "DGND" H 1400 4380 40  0000 C CNN
F 2 "" H 1400 4450 60  0001 C CNN
F 3 "" H 1400 4450 60  0001 C CNN
	1    1400 4450
	1    0    0    -1  
$EndComp
$Comp
L DGND #PWR017
U 1 1 5135FB92
P 1900 4950
F 0 "#PWR017" H 1900 4950 40  0001 C CNN
F 1 "DGND" H 1900 4880 40  0000 C CNN
F 2 "" H 1900 4950 60  0001 C CNN
F 3 "" H 1900 4950 60  0001 C CNN
	1    1900 4950
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5134B4C9
P 1850 5650
F 0 "C4" H 1900 5750 50  0000 L CNN
F 1 "100n" H 1900 5550 50  0000 L CNN
F 2 "" H 1850 5650 60  0001 C CNN
F 3 "" H 1850 5650 60  0001 C CNN
	1    1850 5650
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR018
U 1 1 51349C2B
P 10450 7200
F 0 "#PWR018" H 10450 7200 40  0001 C CNN
F 1 "AGND" H 10450 7130 50  0000 C CNN
F 2 "" H 10450 7200 60  0001 C CNN
F 3 "" H 10450 7200 60  0001 C CNN
	1    10450 7200
	1    0    0    -1  
$EndComp
$Comp
L CP1 C12
U 1 1 51349C21
P 10450 6900
F 0 "C12" H 10500 7000 50  0000 L CNN
F 1 "22u" H 10500 6800 50  0000 L CNN
F 2 "" H 10450 6900 60  0001 C CNN
F 3 "" H 10450 6900 60  0001 C CNN
	1    10450 6900
	1    0    0    -1  
$EndComp
Text GLabel 4150 6200 2    40   Output ~ 0
PWM
$Comp
L R R16
U 1 1 5133B669
P 12800 3550
F 0 "R16" V 12880 3550 50  0000 C CNN
F 1 "10K" V 12800 3550 50  0000 C CNN
F 2 "" H 12800 3550 60  0001 C CNN
F 3 "" H 12800 3550 60  0001 C CNN
	1    12800 3550
	0    1    1    0   
$EndComp
$Comp
L R R18
U 1 1 5133B63E
P 13650 3050
F 0 "R18" V 13730 3050 50  0000 C CNN
F 1 "10K" V 13650 3050 50  0000 C CNN
F 2 "" H 13650 3050 60  0001 C CNN
F 3 "" H 13650 3050 60  0001 C CNN
	1    13650 3050
	0    1    1    0   
$EndComp
Text GLabel 2700 2300 2    40   Output ~ 0
MOSI
Text GLabel 1900 2300 0    40   Output ~ 0
SCK
Text GLabel 1900 2200 0    40   Input ~ 0
MISO
Text GLabel 4600 4350 2    30   Input ~ 0
MOSI
Text GLabel 4600 4250 2    30   Output ~ 0
DAC13
Text GLabel 4600 4450 2    30   Output ~ 0
DAC14
Text GLabel 4600 4550 2    30   Output ~ 0
MISO
Text GLabel 4600 4750 2    30   Input ~ 0
SCK
Text GLabel 4600 4650 2    30   Output ~ 0
DAC15
Text Notes 1750 7800 0    100  ~ 0
OpAmp Decoupling
$Comp
L VDD #PWR019
U 1 1 51336082
P 2750 9250
F 0 "#PWR019" H 2750 9350 30  0001 C CNN
F 1 "VDD" H 2750 9360 30  0000 C CNN
F 2 "" H 2750 9250 60  0001 C CNN
F 3 "" H 2750 9250 60  0001 C CNN
	1    2750 9250
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR020
U 1 1 5133607F
P 2750 8150
F 0 "#PWR020" H 2750 8250 30  0001 C CNN
F 1 "VCC" H 2750 8250 30  0000 C CNN
F 2 "" H 2750 8150 60  0001 C CNN
F 3 "" H 2750 8150 60  0001 C CNN
	1    2750 8150
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 51336079
P 2750 8750
F 0 "C7" H 2800 8850 50  0000 L CNN
F 1 "100n" H 2800 8650 50  0000 L CNN
F 2 "" H 2750 8750 60  0001 C CNN
F 3 "" H 2750 8750 60  0001 C CNN
	1    2750 8750
	1    0    0    -1  
$EndComp
NoConn ~ 7900 6950
NoConn ~ 8700 6950
$Comp
L AGND #PWR021
U 1 1 51335D52
P 10350 7850
F 0 "#PWR021" H 10350 7850 40  0001 C CNN
F 1 "AGND" H 10350 7780 50  0000 C CNN
F 2 "" H 10350 7850 60  0001 C CNN
F 3 "" H 10350 7850 60  0001 C CNN
	1    10350 7850
	1    0    0    -1  
$EndComp
$Comp
L DGND #PWR022
U 1 1 51335D3D
P 6700 2850
F 0 "#PWR022" H 6700 2850 40  0001 C CNN
F 1 "DGND" H 6700 2780 40  0000 C CNN
F 2 "" H 6700 2850 60  0001 C CNN
F 3 "" H 6700 2850 60  0001 C CNN
	1    6700 2850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR023
U 1 1 51335D2F
P 6500 3050
F 0 "#PWR023" H 6500 3140 20  0001 C CNN
F 1 "+5V" H 6500 3140 30  0000 C CNN
F 2 "" H 6500 3050 60  0001 C CNN
F 3 "" H 6500 3050 60  0001 C CNN
	1    6500 3050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR024
U 1 1 51335B59
P 10700 6650
F 0 "#PWR024" H 10700 6740 20  0001 C CNN
F 1 "+5V" H 10700 6740 30  0000 C CNN
F 2 "" H 10700 6650 60  0001 C CNN
F 3 "" H 10700 6650 60  0001 C CNN
	1    10700 6650
	1    0    0    -1  
$EndComp
$Comp
L CP1 C10
U 1 1 51335B1C
P 10050 8000
F 0 "C10" H 10100 8100 50  0000 L CNN
F 1 "22u" H 10100 7900 50  0000 L CNN
F 2 "" H 10050 8000 60  0001 C CNN
F 3 "" H 10050 8000 60  0001 C CNN
	1    10050 8000
	1    0    0    -1  
$EndComp
$Comp
L CP1 C9
U 1 1 51335B11
P 10050 7500
F 0 "C9" H 10100 7600 50  0000 L CNN
F 1 "22u" H 10100 7400 50  0000 L CNN
F 2 "" H 10050 7500 60  0001 C CNN
F 3 "" H 10050 7500 60  0001 C CNN
	1    10050 7500
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 51335AEE
P 9700 8250
F 0 "R7" V 9780 8250 50  0000 C CNN
F 1 "10" V 9700 8250 50  0000 C CNN
F 2 "" H 9700 8250 60  0001 C CNN
F 3 "" H 9700 8250 60  0001 C CNN
	1    9700 8250
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 51335AA5
P 9700 7250
F 0 "R6" V 9780 7250 50  0000 C CNN
F 1 "10" V 9700 7250 50  0000 C CNN
F 2 "" H 9700 7250 60  0001 C CNN
F 3 "" H 9700 7250 60  0001 C CNN
	1    9700 7250
	0    1    1    0   
$EndComp
$Comp
L VDD #PWR025
U 1 1 51335A76
P 10050 8350
F 0 "#PWR025" H 10050 8450 30  0001 C CNN
F 1 "VDD" H 10050 8460 30  0000 C CNN
F 2 "" H 10050 8350 60  0001 C CNN
F 3 "" H 10050 8350 60  0001 C CNN
	1    10050 8350
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR026
U 1 1 51335A50
P 10050 7100
F 0 "#PWR026" H 10050 7200 30  0001 C CNN
F 1 "VCC" H 10050 7200 30  0000 C CNN
F 2 "" H 10050 7100 60  0001 C CNN
F 3 "" H 10050 7100 60  0001 C CNN
	1    10050 7100
	1    0    0    -1  
$EndComp
Text Notes 7500 7900 0    100  ~ 0
Eurorack Connector
$Comp
L AGND #PWR027
U 1 1 513357B7
P 9000 3450
F 0 "#PWR027" H 9000 3450 40  0001 C CNN
F 1 "AGND" H 9000 3380 50  0000 C CNN
F 2 "" H 9000 3450 60  0001 C CNN
F 3 "" H 9000 3450 60  0001 C CNN
	1    9000 3450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR028
U 1 1 5133579F
P 8000 3800
F 0 "#PWR028" H 8000 3890 20  0001 C CNN
F 1 "+5V" H 8000 3900 30  0000 C CNN
F 2 "" H 8000 3800 60  0001 C CNN
F 3 "" H 8000 3800 60  0001 C CNN
	1    8000 3800
	-1   0    0    1   
$EndComp
$Comp
L VDD #PWR029
U 1 1 51335719
P 7500 3800
F 0 "#PWR029" H 7500 3900 30  0001 C CNN
F 1 "VDD" H 7500 3910 30  0000 C CNN
F 2 "" H 7500 3800 60  0001 C CNN
F 3 "" H 7500 3800 60  0001 C CNN
	1    7500 3800
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR030
U 1 1 51335715
P 7750 3800
F 0 "#PWR030" H 7750 3900 30  0001 C CNN
F 1 "VCC" H 7750 3900 30  0000 C CNN
F 2 "" H 7750 3800 60  0001 C CNN
F 3 "" H 7750 3800 60  0001 C CNN
	1    7750 3800
	-1   0    0    1   
$EndComp
$Comp
L CONN_8X2 J3
U 1 1 5132A266
P 8300 7300
F 0 "J3" H 8300 7750 60  0000 C CNN
F 1 "EURORACK CONNECTOR" V 8300 7300 50  0000 C CNN
F 2 "" H 8300 7300 60  0001 C CNN
F 3 "" H 8300 7300 60  0001 C CNN
	1    8300 7300
	1    0    0    1   
$EndComp
Text Notes 11350 2750 0    100  ~ 0
DC Remover
$Comp
L VDD #PWR031
U 1 1 5132A18B
P 10500 3200
F 0 "#PWR031" H 10500 3300 30  0001 C CNN
F 1 "VDD" H 10500 3310 30  0000 C CNN
F 2 "" H 10500 3200 60  0001 C CNN
F 3 "" H 10500 3200 60  0001 C CNN
	1    10500 3200
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR032
U 1 1 5132A183
P 10500 2700
F 0 "#PWR032" H 10500 2800 30  0001 C CNN
F 1 "VCC" H 10500 2800 30  0000 C CNN
F 2 "" H 10500 2700 60  0001 C CNN
F 3 "" H 10500 2700 60  0001 C CNN
	1    10500 2700
	1    0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 5132A155
P 11000 2950
F 0 "R14" V 11080 2950 50  0000 C CNN
F 1 "5K" V 11000 2950 50  0000 C CNN
F 2 "" H 11000 2950 60  0001 C CNN
F 3 "" H 11000 2950 60  0001 C CNN
	1    11000 2950
	0    1    1    0   
$EndComp
$Comp
L R R17
U 1 1 5132A151
P 11900 2950
F 0 "R17" V 11980 2950 50  0000 C CNN
F 1 "10K" V 11900 2950 50  0000 C CNN
F 2 "" H 11900 2950 60  0001 C CNN
F 3 "" H 11900 2950 60  0001 C CNN
	1    11900 2950
	0    1    1    0   
$EndComp
$Comp
L R R15
U 1 1 5132A141
P 11000 3450
F 0 "R15" V 11080 3450 50  0000 C CNN
F 1 "10K" V 11000 3450 50  0000 C CNN
F 2 "" H 11000 3450 60  0001 C CNN
F 3 "" H 11000 3450 60  0001 C CNN
	1    11000 3450
	0    1    1    0   
$EndComp
$Comp
L AGND #PWR033
U 1 1 5132A133
P 11550 3900
F 0 "#PWR033" H 11550 3900 40  0001 C CNN
F 1 "AGND" H 11550 3830 50  0000 C CNN
F 2 "" H 11550 3900 60  0001 C CNN
F 3 "" H 11550 3900 60  0001 C CNN
	1    11550 3900
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 5132A117
P 2350 8750
F 0 "C6" H 2400 8850 50  0000 L CNN
F 1 "100n" H 2350 8650 50  0000 L CNN
F 2 "" H 2350 8750 60  0001 C CNN
F 3 "" H 2350 8750 60  0001 C CNN
	1    2350 8750
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR034
U 1 1 5132A116
P 2350 9250
F 0 "#PWR034" H 2350 9350 30  0001 C CNN
F 1 "VDD" H 2350 9360 30  0000 C CNN
F 2 "" H 2350 9250 60  0001 C CNN
F 3 "" H 2350 9250 60  0001 C CNN
	1    2350 9250
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR035
U 1 1 5132A115
P 2350 8150
F 0 "#PWR035" H 2350 8250 30  0001 C CNN
F 1 "VCC" H 2350 8250 30  0000 C CNN
F 2 "" H 2350 8150 60  0001 C CNN
F 3 "" H 2350 8150 60  0001 C CNN
	1    2350 8150
	1    0    0    -1  
$EndComp
$Comp
L POT T2
U 1 1 5132A0F5
P 10500 2950
F 0 "T2" H 10500 2850 50  0000 C CNN
F 1 "10K" H 10500 2950 50  0000 C CNN
F 2 "" H 10500 2950 60  0001 C CNN
F 3 "" H 10500 2950 60  0001 C CNN
	1    10500 2950
	0    1    1    0   
$EndComp
$Comp
L TL072 U?
U 2 1 51329D11
P 12050 3550
AR Path="/51326B47" Ref="U?"  Part="1" 
AR Path="/51326B51" Ref="U?"  Part="1" 
AR Path="/51328582" Ref="U?"  Part="1" 
AR Path="/51329D11" Ref="U2"  Part="2" 
F 0 "U2" H 12000 3750 60  0000 L CNN
F 1 "TL072" H 12000 3300 60  0000 L CNN
F 2 "" H 12050 3550 60  0001 C CNN
F 3 "" H 12050 3550 60  0001 C CNN
	2    12050 3550
	1    0    0    1   
$EndComp
$Comp
L R R20
U 1 1 51329D0F
P 14600 3650
F 0 "R20" V 14680 3650 50  0000 C CNN
F 1 "1K" V 14600 3650 50  0000 C CNN
F 2 "" H 14600 3650 60  0001 C CNN
F 3 "" H 14600 3650 60  0001 C CNN
	1    14600 3650
	0    1    1    0   
$EndComp
$Comp
L AGND #PWR036
U 1 1 51329CB7
P 13150 4050
F 0 "#PWR036" H 13150 4050 40  0001 C CNN
F 1 "AGND" H 13150 3980 50  0000 C CNN
F 2 "" H 13150 4050 60  0001 C CNN
F 3 "" H 13150 4050 60  0001 C CNN
	1    13150 4050
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR037
U 1 1 513299AC
P 6000 3400
F 0 "#PWR037" H 6000 3400 40  0001 C CNN
F 1 "AGND" H 6000 3330 50  0000 C CNN
F 2 "" H 6000 3400 60  0001 C CNN
F 3 "" H 6000 3400 60  0001 C CNN
	1    6000 3400
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR038
U 1 1 513299AB
P 5550 3400
F 0 "#PWR038" H 5550 3400 40  0001 C CNN
F 1 "AGND" H 5550 3330 50  0000 C CNN
F 2 "" H 5550 3400 60  0001 C CNN
F 3 "" H 5550 3400 60  0001 C CNN
	1    5550 3400
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR039
U 1 1 513299A9
P 5100 3400
F 0 "#PWR039" H 5100 3400 40  0001 C CNN
F 1 "AGND" H 5100 3330 50  0000 C CNN
F 2 "" H 5100 3400 60  0001 C CNN
F 3 "" H 5100 3400 60  0001 C CNN
	1    5100 3400
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR040
U 1 1 51329978
P 4650 3400
F 0 "#PWR040" H 4650 3400 40  0001 C CNN
F 1 "AGND" H 4650 3330 50  0000 C CNN
F 2 "" H 4650 3400 60  0001 C CNN
F 3 "" H 4650 3400 60  0001 C CNN
	1    4650 3400
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR041
U 1 1 51329933
P 11100 5600
F 0 "#PWR041" H 11100 5600 40  0001 C CNN
F 1 "AGND" H 11100 5530 50  0000 C CNN
F 2 "" H 11100 5600 60  0001 C CNN
F 3 "" H 11100 5600 60  0001 C CNN
	1    11100 5600
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR042
U 1 1 5132992D
P 10450 5600
F 0 "#PWR042" H 10450 5600 40  0001 C CNN
F 1 "AGND" H 10450 5530 50  0000 C CNN
F 2 "" H 10450 5600 60  0001 C CNN
F 3 "" H 10450 5600 60  0001 C CNN
	1    10450 5600
	1    0    0    -1  
$EndComp
$Comp
L DGND #PWR043
U 1 1 5132990E
P 8250 3800
F 0 "#PWR043" H 8250 3800 40  0001 C CNN
F 1 "DGND" H 8250 3700 40  0000 C CNN
F 2 "" H 8250 3800 60  0001 C CNN
F 3 "" H 8250 3800 60  0001 C CNN
	1    8250 3800
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR044
U 1 1 513298DB
P 3300 6950
F 0 "#PWR044" H 3300 6950 40  0001 C CNN
F 1 "AGND" H 3300 6880 50  0000 C CNN
F 2 "" H 3300 6950 60  0001 C CNN
F 3 "" H 3300 6950 60  0001 C CNN
	1    3300 6950
	1    0    0    -1  
$EndComp
$Comp
L DGND #PWR045
U 1 1 513298D7
P 3000 6950
F 0 "#PWR045" H 3000 6950 40  0001 C CNN
F 1 "DGND" H 3000 6880 40  0000 C CNN
F 2 "" H 3000 6950 60  0001 C CNN
F 3 "" H 3000 6950 60  0001 C CNN
	1    3000 6950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR046
U 1 1 5132989A
P 2750 2100
F 0 "#PWR046" H 2750 2190 20  0001 C CNN
F 1 "+5V" H 2750 2190 30  0000 C CNN
F 2 "" H 2750 2100 60  0001 C CNN
F 3 "" H 2750 2100 60  0001 C CNN
	1    2750 2100
	1    0    0    -1  
$EndComp
Text Notes 9200 6000 0    100  ~ 0
MFB Filter (14KHz - LP)
$Comp
L CONN_3X2 J1
U 1 1 5132972F
P 2300 2350
F 0 "J1" H 2300 2600 50  0000 C CNN
F 1 "ICSP" H 2300 2300 40  0000 C CNN
F 2 "" H 2300 2350 60  0001 C CNN
F 3 "" H 2300 2350 60  0001 C CNN
	1    2300 2350
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 51329027
P 10750 5000
F 0 "R13" V 10830 5000 50  0000 C CNN
F 1 "8K5" V 10750 5000 50  0000 C CNN
F 2 "" H 10750 5000 60  0001 C CNN
F 3 "" H 10750 5000 60  0001 C CNN
	1    10750 5000
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 51328FCC
P 10150 5000
F 0 "R10" V 10230 5000 50  0000 C CNN
F 1 "150K" V 10150 5000 50  0000 C CNN
F 2 "" H 10150 5000 60  0001 C CNN
F 3 "" H 10150 5000 60  0001 C CNN
	1    10150 5000
	0    1    1    0   
$EndComp
$Comp
L C C14
U 1 1 51328FBB
P 11050 4700
F 0 "C14" H 11100 4800 50  0000 L CNN
F 1 "1n" H 11100 4600 50  0000 L CNN
F 2 "" H 11050 4700 60  0001 C CNN
F 3 "" H 11050 4700 60  0001 C CNN
	1    11050 4700
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 51328677
P 12450 5100
F 0 "R19" V 12530 5100 50  0000 C CNN
F 1 "1K" V 12450 5100 50  0000 C CNN
F 2 "" H 12450 5100 60  0001 C CNN
F 3 "" H 12450 5100 60  0001 C CNN
	1    12450 5100
	0    1    1    0   
$EndComp
$Comp
L C C13
U 1 1 51328608
P 10450 5250
F 0 "C13" H 10500 5350 50  0000 L CNN
F 1 "100p" H 10500 5150 50  0000 L CNN
F 2 "" H 10450 5250 60  0001 C CNN
F 3 "" H 10450 5250 60  0001 C CNN
	1    10450 5250
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 513285EB
P 10450 4700
F 0 "R12" V 10530 4700 50  0000 C CNN
F 1 "150K" V 10450 4700 50  0000 C CNN
F 2 "" H 10450 4700 60  0001 C CNN
F 3 "" H 10450 4700 60  0001 C CNN
	1    10450 4700
	-1   0    0    1   
$EndComp
$Comp
L TL072 U?
U 2 1 51328590
P 11600 5100
AR Path="/51326B47" Ref="U?"  Part="1" 
AR Path="/51326B51" Ref="U?"  Part="1" 
AR Path="/51328582" Ref="U?"  Part="1" 
AR Path="/51328590" Ref="U1"  Part="2" 
F 0 "U1" H 11550 5300 60  0000 L CNN
F 1 "TL072" H 11550 4850 60  0000 L CNN
F 2 "" H 11600 5100 60  0001 C CNN
F 3 "" H 11600 5100 60  0001 C CNN
	2    11600 5100
	1    0    0    1   
$EndComp
$Comp
L TL072 U?
U 1 1 51328582
P 13650 3650
AR Path="/51326B47" Ref="U?"  Part="1" 
AR Path="/51326B51" Ref="U?"  Part="1" 
AR Path="/51328582" Ref="U2"  Part="1" 
F 0 "U2" H 13600 3850 60  0000 L CNN
F 1 "TL072" H 13600 3400 60  0000 L CNN
F 2 "" H 13650 3650 60  0001 C CNN
F 3 "" H 13650 3650 60  0001 C CNN
	1    13650 3650
	1    0    0    1   
$EndComp
Text GLabel 4150 4300 2    40   Output ~ 0
DAC12
Text GLabel 4150 4200 2    40   Output ~ 0
DAC11
Text GLabel 4150 4100 2    40   Output ~ 0
DAC10
Text GLabel 4150 4000 2    40   Output ~ 0
DAC9
Text GLabel 4150 3900 2    40   Output ~ 0
DAC8
Text GLabel 4150 5400 2    40   Output ~ 0
DAC6
Text GLabel 4150 5300 2    40   Output ~ 0
DAC5
Text GLabel 4150 5200 2    40   Output ~ 0
DAC4
Text GLabel 4150 5100 2    40   Output ~ 0
DAC3
Text GLabel 4150 5000 2    40   Output ~ 0
DAC2
Text GLabel 4150 4900 2    40   Output ~ 0
DAC1
Text GLabel 4150 4800 2    40   Output ~ 0
DAC0
Text GLabel 8600 2200 1    40   Input ~ 0
DAC0
Text GLabel 8500 2200 1    40   Input ~ 0
DAC1
Text GLabel 8400 2200 1    40   Input ~ 0
DAC2
Text GLabel 8300 2200 1    40   Input ~ 0
DAC3
Text GLabel 8200 2200 1    40   Input ~ 0
DAC4
Text GLabel 8100 2200 1    40   Input ~ 0
DAC5
Text GLabel 8000 2200 1    40   Input ~ 0
DAC6
Text GLabel 7900 2200 1    40   Input ~ 0
DAC7
Text GLabel 7800 2200 1    40   Input ~ 0
DAC8
Text GLabel 7700 2200 1    40   Input ~ 0
DAC9
Text GLabel 7600 2200 1    40   Input ~ 0
DAC10
Text GLabel 7500 2200 1    40   Input ~ 0
DAC11
Text GLabel 7400 2200 1    40   Input ~ 0
DAC12
Text GLabel 7300 2200 1    40   Input ~ 0
DAC13
Text GLabel 7200 2200 1    40   Input ~ 0
DAC14
Text GLabel 7100 2200 1    40   Input ~ 0
DAC15
$Comp
L POT T1
U 1 1 51328273
P 6500 3500
F 0 "T1" H 6500 3400 50  0000 C CNN
F 1 "100" H 6500 3500 50  0000 C CNN
F 2 "" H 6500 3500 60  0001 C CNN
F 3 "" H 6500 3500 60  0001 C CNN
	1    6500 3500
	0    1    1    0   
$EndComp
$Comp
L AD669 U4
U 1 1 5132809E
P 7850 2950
F 0 "U4" H 8700 2200 60  0000 C CNN
F 1 "AD669" H 7800 3000 60  0000 C CNN
F 2 "" H 7850 2950 60  0001 C CNN
F 3 "" H 7850 2950 60  0001 C CNN
	1    7850 2950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR047
U 1 1 51327424
P 6000 2600
F 0 "#PWR047" H 6000 2690 20  0001 C CNN
F 1 "+5V" H 6000 2690 30  0000 C CNN
F 2 "" H 6000 2600 60  0001 C CNN
F 3 "" H 6000 2600 60  0001 C CNN
	1    6000 2600
	1    0    0    -1  
$EndComp
$Comp
L POT P4
U 1 1 51327423
P 6000 3000
F 0 "P4" H 6000 2900 50  0000 C CNN
F 1 "10K" H 6000 3000 50  0000 C CNN
F 2 "" H 6000 3000 60  0001 C CNN
F 3 "" H 6000 3000 60  0001 C CNN
	1    6000 3000
	0    -1   1    0   
$EndComp
$Comp
L POT P3
U 1 1 513273CB
P 5550 3000
F 0 "P3" H 5550 2900 50  0000 C CNN
F 1 "10K" H 5550 3000 50  0000 C CNN
F 2 "" H 5550 3000 60  0001 C CNN
F 3 "" H 5550 3000 60  0001 C CNN
	1    5550 3000
	0    -1   1    0   
$EndComp
$Comp
L +5V #PWR048
U 1 1 513273CA
P 5550 2600
F 0 "#PWR048" H 5550 2690 20  0001 C CNN
F 1 "+5V" H 5550 2690 30  0000 C CNN
F 2 "" H 5550 2600 60  0001 C CNN
F 3 "" H 5550 2600 60  0001 C CNN
	1    5550 2600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR049
U 1 1 513273B9
P 5100 2600
F 0 "#PWR049" H 5100 2690 20  0001 C CNN
F 1 "+5V" H 5100 2690 30  0000 C CNN
F 2 "" H 5100 2600 60  0001 C CNN
F 3 "" H 5100 2600 60  0001 C CNN
	1    5100 2600
	1    0    0    -1  
$EndComp
$Comp
L POT P2
U 1 1 513273B8
P 5100 3000
F 0 "P2" H 5100 2900 50  0000 C CNN
F 1 "10K" H 5100 3000 50  0000 C CNN
F 2 "" H 5100 3000 60  0001 C CNN
F 3 "" H 5100 3000 60  0001 C CNN
	1    5100 3000
	0    -1   1    0   
$EndComp
$Comp
L POT P1
U 1 1 51327327
P 4650 3000
F 0 "P1" H 4650 2900 50  0000 C CNN
F 1 "10K" H 4650 3000 50  0000 C CNN
F 2 "" H 4650 3000 60  0001 C CNN
F 3 "" H 4650 3000 60  0001 C CNN
	1    4650 3000
	0    -1   1    0   
$EndComp
$Comp
L +5V #PWR050
U 1 1 51327322
P 4650 2600
F 0 "#PWR050" H 4650 2690 20  0001 C CNN
F 1 "+5V" H 4650 2690 30  0000 C CNN
F 2 "" H 4650 2600 60  0001 C CNN
F 3 "" H 4650 2600 60  0001 C CNN
	1    4650 2600
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5132700E
P 1900 4550
F 0 "C5" H 1950 4650 50  0000 L CNN
F 1 "100n" H 1950 4450 50  0000 L CNN
F 2 "" H 1900 4550 60  0001 C CNN
F 3 "" H 1900 4550 60  0001 C CNN
	1    1900 4550
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 51326F9B
P 1500 3000
F 0 "R1" V 1580 3000 50  0000 C CNN
F 1 "10K" V 1500 3000 50  0000 C CNN
F 2 "" H 1500 3000 60  0001 C CNN
F 3 "" H 1500 3000 60  0001 C CNN
	1    1500 3000
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR051
U 1 1 51326F95
P 1150 2850
F 0 "#PWR051" H 1150 2940 20  0001 C CNN
F 1 "+5V" H 1150 2940 30  0000 C CNN
F 2 "" H 1150 2850 60  0001 C CNN
F 3 "" H 1150 2850 60  0001 C CNN
	1    1150 2850
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 51326F4D
P 1400 4150
F 0 "C2" H 1450 4250 50  0000 L CNN
F 1 "22p" H 1450 4050 50  0000 L CNN
F 2 "" H 1400 4150 60  0001 C CNN
F 3 "" H 1400 4150 60  0001 C CNN
	1    1400 4150
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 51326F4A
P 800 4150
F 0 "C1" H 850 4250 50  0000 L CNN
F 1 "22p" H 850 4050 50  0000 L CNN
F 2 "" H 800 4150 60  0001 C CNN
F 3 "" H 800 4150 60  0001 C CNN
	1    800  4150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR052
U 1 1 51326C64
P 3150 2450
F 0 "#PWR052" H 3150 2540 20  0001 C CNN
F 1 "+5V" H 3150 2540 30  0000 C CNN
F 2 "" H 3150 2450 60  0001 C CNN
F 3 "" H 3150 2450 60  0001 C CNN
	1    3150 2450
	1    0    0    -1  
$EndComp
Text GLabel 4150 3400 2    40   Input ~ 0
ADC3
Text GLabel 4150 3500 2    40   Input ~ 0
ADC2
Text GLabel 4150 3600 2    40   Input ~ 0
ADC1
Text GLabel 4150 3700 2    40   Input ~ 0
ADC0
$Comp
L C C3
U 1 1 51326BBE
P 1950 8750
F 0 "C3" H 2000 8850 50  0000 L CNN
F 1 "100n" H 1950 8650 50  0000 L CNN
F 2 "" H 1950 8750 60  0001 C CNN
F 3 "" H 1950 8750 60  0001 C CNN
	1    1950 8750
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR053
U 1 1 51326BBD
P 1950 8150
F 0 "#PWR053" H 1950 8250 30  0001 C CNN
F 1 "VCC" H 1950 8250 30  0000 C CNN
F 2 "" H 1950 8150 60  0001 C CNN
F 3 "" H 1950 8150 60  0001 C CNN
	1    1950 8150
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR054
U 1 1 51326BBC
P 1950 9250
F 0 "#PWR054" H 1950 9350 30  0001 C CNN
F 1 "VDD" H 1950 9360 30  0000 C CNN
F 2 "" H 1950 9250 60  0001 C CNN
F 3 "" H 1950 9250 60  0001 C CNN
	1    1950 9250
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 513265E0
P 5350 5550
F 0 "R2" V 5430 5550 50  0000 C CNN
F 1 "220" V 5350 5550 50  0000 C CNN
F 2 "" H 5350 5550 60  0001 C CNN
F 3 "" H 5350 5550 60  0001 C CNN
	1    5350 5550
	1    0    0    -1  
$EndComp
$Comp
L CRYSTAL X1
U 1 1 5132636E
P 1100 3800
F 0 "X1" H 1100 3950 60  0000 C CNN
F 1 "16MHz" H 1100 3650 60  0000 C CNN
F 2 "" H 1100 3800 60  0001 C CNN
F 3 "" H 1100 3800 60  0001 C CNN
	1    1100 3800
	-1   0    0    1   
$EndComp
$Comp
L ATMEGA16-P U3
U 1 1 51326324
P 3150 4700
F 0 "U3" H 2350 6430 50  0000 L BNN
F 1 "ATMEGA1284P" H 2800 4750 50  0000 L BNN
F 2 "DIL40" H 3650 2625 50  0001 C CNN
F 3 "" H 3150 4700 60  0001 C CNN
	1    3150 4700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR057
U 1 1 51D8B27B
P 11500 5500
F 0 "#PWR057" H 11500 5600 30  0001 C CNN
F 1 "VCC" H 11500 5600 30  0000 C CNN
F 2 "" H 11500 5500 60  0001 C CNN
F 3 "" H 11500 5500 60  0001 C CNN
	1    11500 5500
	-1   0    0    1   
$EndComp
$Comp
L VDD #PWR060
U 1 1 51D8B2B3
P 11500 4700
F 0 "#PWR060" H 11500 4800 30  0001 C CNN
F 1 "VDD" H 11500 4810 30  0000 C CNN
F 2 "" H 11500 4700 60  0001 C CNN
F 3 "" H 11500 4700 60  0001 C CNN
	1    11500 4700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 51D8B4D7
P 9200 5400
F 0 "#PWR?" H 9200 5500 30  0001 C CNN
F 1 "VCC" H 9200 5500 30  0000 C CNN
F 2 "" H 9200 5400 60  0001 C CNN
F 3 "" H 9200 5400 60  0001 C CNN
	1    9200 5400
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR?
U 1 1 51D8B4DD
P 11950 3950
F 0 "#PWR?" H 11950 4050 30  0001 C CNN
F 1 "VCC" H 11950 4050 30  0000 C CNN
F 2 "" H 11950 3950 60  0001 C CNN
F 3 "" H 11950 3950 60  0001 C CNN
	1    11950 3950
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR?
U 1 1 51D8B4E3
P 13550 4050
F 0 "#PWR?" H 13550 4150 30  0001 C CNN
F 1 "VCC" H 13550 4150 30  0000 C CNN
F 2 "" H 13550 4050 60  0001 C CNN
F 3 "" H 13550 4050 60  0001 C CNN
	1    13550 4050
	-1   0    0    1   
$EndComp
$Comp
L VDD #PWR?
U 1 1 51D8B4E9
P 9200 4600
F 0 "#PWR?" H 9200 4700 30  0001 C CNN
F 1 "VDD" H 9200 4710 30  0000 C CNN
F 2 "" H 9200 4600 60  0001 C CNN
F 3 "" H 9200 4600 60  0001 C CNN
	1    9200 4600
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR?
U 1 1 51D8B4EF
P 11950 3150
F 0 "#PWR?" H 11950 3250 30  0001 C CNN
F 1 "VDD" H 11950 3260 30  0000 C CNN
F 2 "" H 11950 3150 60  0001 C CNN
F 3 "" H 11950 3150 60  0001 C CNN
	1    11950 3150
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR?
U 1 1 51D8B4F5
P 13550 3250
F 0 "#PWR?" H 13550 3350 30  0001 C CNN
F 1 "VDD" H 13550 3360 30  0000 C CNN
F 2 "" H 13550 3250 60  0001 C CNN
F 3 "" H 13550 3250 60  0001 C CNN
	1    13550 3250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
